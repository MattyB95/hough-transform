# Hough Transform

A program to perform hough transform to help me solve the below questions.

![alt text](res/hough-transform-questions.jpg "Hough Transform Questions")

# Program Output and Question Answers

 ----- Question One ----- 

For edge point at A(1, 1):
ρ=1cos(-90°)+1sin(-90°)=-1.0
ρ=1cos(-60°)+1sin(-60°)=-0.5
ρ=1cos(-45°)+1sin(-45°)=0.0
ρ=1cos(-30°)+1sin(-30°)=0.5
ρ=1cos(0°)+1sin(0°)=1.0
ρ=1cos(30°)+1sin(30°)=1.5
ρ=1cos(45°)+1sin(45°)=1.5
ρ=1cos(60°)+1sin(60°)=1.5
ρ=1cos(90°)+1sin(90°)=1.0

For edge point at B(2, 2):
ρ=2cos(-90°)+2sin(-90°)=-2.0
ρ=2cos(-60°)+2sin(-60°)=-0.5
ρ=2cos(-45°)+2sin(-45°)=0.0
ρ=2cos(-30°)+2sin(-30°)=0.5
ρ=2cos(0°)+2sin(0°)=2.0
ρ=2cos(30°)+2sin(30°)=2.5
ρ=2cos(45°)+2sin(45°)=3.0
ρ=2cos(60°)+2sin(60°)=2.5
ρ=2cos(90°)+2sin(90°)=2.0

For edge point at C(1, 3):
ρ=1cos(-90°)+3sin(-90°)=-3.0
ρ=1cos(-60°)+3sin(-60°)=-2.0
ρ=1cos(-45°)+3sin(-45°)=-1.5
ρ=1cos(-30°)+3sin(-30°)=-0.5
ρ=1cos(0°)+3sin(0°)=1.0
ρ=1cos(30°)+3sin(30°)=2.5
ρ=1cos(45°)+3sin(45°)=3.0
ρ=1cos(60°)+3sin(60°)=3.0
ρ=1cos(90°)+3sin(90°)=3.0

For edge point at D(3, 1):
ρ=3cos(-90°)+1sin(-90°)=-1.0
ρ=3cos(-60°)+1sin(-60°)=0.5
ρ=3cos(-45°)+1sin(-45°)=1.5
ρ=3cos(-30°)+1sin(-30°)=2.0
ρ=3cos(0°)+1sin(0°)=3.0
ρ=3cos(30°)+1sin(30°)=3.0
ρ=3cos(45°)+1sin(45°)=3.0
ρ=3cos(60°)+1sin(60°)=2.5
ρ=3cos(90°)+1sin(90°)=1.0

Points coincide:
Theta: 45° ---------> Rho: 3.0

The equation of this straight line is, therefore:
3.0=xcos(45°)+ysin(45°)

 ----- Question Two ----- 

For edge point at A(2, 3):
ρ=2cos(-90°)+3sin(-90°)=-3.0
ρ=2cos(-60°)+3sin(-60°)=-1.5
ρ=2cos(-45°)+3sin(-45°)=-0.5
ρ=2cos(-30°)+3sin(-30°)=0.0
ρ=2cos(0°)+3sin(0°)=2.0
ρ=2cos(30°)+3sin(30°)=3.0
ρ=2cos(45°)+3sin(45°)=3.5
ρ=2cos(60°)+3sin(60°)=3.5
ρ=2cos(90°)+3sin(90°)=3.0

For edge point at B(3, 2):
ρ=3cos(-90°)+2sin(-90°)=-2.0
ρ=3cos(-60°)+2sin(-60°)=0.0
ρ=3cos(-45°)+2sin(-45°)=0.5
ρ=3cos(-30°)+2sin(-30°)=1.5
ρ=3cos(0°)+2sin(0°)=3.0
ρ=3cos(30°)+2sin(30°)=3.5
ρ=3cos(45°)+2sin(45°)=3.5
ρ=3cos(60°)+2sin(60°)=3.0
ρ=3cos(90°)+2sin(90°)=2.0

For edge point at C(1, 4):
ρ=1cos(-90°)+4sin(-90°)=-4.0
ρ=1cos(-60°)+4sin(-60°)=-3.0
ρ=1cos(-45°)+4sin(-45°)=-2.0
ρ=1cos(-30°)+4sin(-30°)=-1.0
ρ=1cos(0°)+4sin(0°)=1.0
ρ=1cos(30°)+4sin(30°)=3.0
ρ=1cos(45°)+4sin(45°)=3.5
ρ=1cos(60°)+4sin(60°)=4.0
ρ=1cos(90°)+4sin(90°)=4.0

Points coincide:
Theta: 45° ---------> Rho: 3.5

The equation of this straight line is, therefore:
3.5=xcos(45°)+ysin(45°)