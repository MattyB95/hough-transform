import java.util.Objects;

public class AccumulatorInput {

    private int theta;
    private double rho;

    AccumulatorInput(int theta, double rho) {
        this.theta = theta;
        this.rho = rho;
    }

    int getTheta() {
        return theta;
    }

    double getRho() {
        return rho;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) return true;
        if (!(obj instanceof AccumulatorInput)) {
            return false;
        }
        AccumulatorInput accumulatorInput = (AccumulatorInput) obj;
        return theta == accumulatorInput.theta && rho == accumulatorInput.rho;
    }

    @Override
    public int hashCode() {
        return Objects.hash(theta, rho);
    }

    @Override
    public String toString() {
        return "Theta: " + theta + "° ---------> Rho: " + rho;
    }

}
