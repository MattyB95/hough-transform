class Point {

    private String letter;
    private int x;
    private int y;

    Point(String letter, int x, int y) {
        this.letter = letter;
        this.x = x;
        this.y = y;
    }

    String getLetter() {
        return letter;
    }

    int getX() {
        return x;
    }

    int getY() {
        return y;
    }

}
