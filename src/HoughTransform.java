import java.util.*;
import java.util.stream.Collectors;

public class HoughTransform {

    private static final List<Integer> THETA = Arrays.asList(-90, -60, -45, -30, 0, 30, 45, 60, 90);
    private static final int MINIMUM_REQUIRED_COINCIDE = 3;

    public static void main(String[] args) {
        new HoughTransform();
    }

    private HoughTransform() {
        System.out.println(" ----- Question One ----- ");
        System.out.println();
        performHoughTransform(createPointsQuestionOne());
        System.out.println(" ----- Question Two ----- ");
        System.out.println();
        performHoughTransform(createPointsQuestionTwo());
    }

    private void performHoughTransform(List<Point> points) {
        Map<AccumulatorInput, Integer> accumulator = new HashMap<>();

        for (Point point : points) {
            calculateRhos(accumulator, point);
        }

        accumulator.entrySet().stream()
                .filter(entry -> Objects.equals(entry.getValue(), MINIMUM_REQUIRED_COINCIDE))
                .map(Map.Entry::getKey)
                .collect(Collectors.toSet())
                .forEach(this::displayFoundInformation);
        System.out.println();
    }

    private List<Point> createPointsQuestionOne() {
        Point pointA = new Point("A", 1, 1);
        Point pointB = new Point("B", 2, 2);
        Point pointC = new Point("C", 1, 3);
        Point pointD = new Point("D", 3, 1);
        return Arrays.asList(pointA, pointB, pointC, pointD);
    }

    private List<Point> createPointsQuestionTwo() {
        Point pointA = new Point("A", 2, 3);
        Point pointB = new Point("B", 3, 2);
        Point pointC = new Point("C", 1, 4);
        return Arrays.asList(pointA, pointB, pointC);
    }

    private void calculateRhos(Map<AccumulatorInput, Integer> accumulator, Point point) {
        System.out.println("For edge point at " + point.getLetter() + "(" + point.getX() + ", " + point.getY() + "):");
        for (Integer theta : THETA) {
            AccumulatorInput accumulatorInput = getAccumulatorInput(point, theta);
            addToAccumulator(accumulator, accumulatorInput);
        }
        System.out.println();
    }

    private AccumulatorInput getAccumulatorInput(Point point, Integer theta) {
        double radians = Math.toRadians(theta);
        double rho = (point.getX() * Math.cos(radians)) + (point.getY() * Math.sin(radians));
        double roundedRho = roundToHalf(rho);
        System.out.println("ρ=" + point.getX() + "cos(" + theta + "°)+" + point.getY() + "sin(" + theta + "°)=" + roundedRho);
        return new AccumulatorInput(theta, roundedRho);
    }

    private static double roundToHalf(double d) {
        return Math.round(d * 2) / 2.0;
    }

    private void addToAccumulator(Map<AccumulatorInput, Integer> accumulator, AccumulatorInput accumulatorInput) {
        if (accumulator.containsKey(accumulatorInput)) {
            accumulator.put(accumulatorInput, accumulator.get(accumulatorInput) + 1);
        } else {
            accumulator.put(accumulatorInput, 1);
        }
    }

    private void displayFoundInformation(AccumulatorInput accumulatorInput) {
        System.out.println("Points coincide:");
        System.out.println(accumulatorInput);
        System.out.println();
        System.out.println("The equation of this straight line is, therefore:");
        double rho = accumulatorInput.getRho();
        int theta = accumulatorInput.getTheta();
        System.out.println(rho + "=xcos(" + theta + "°)+ysin(" + theta + "°)");
    }

}
